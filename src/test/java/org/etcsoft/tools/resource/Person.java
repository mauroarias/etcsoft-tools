package org.etcsoft.tools.resource;

import lombok.Data;

@Data
public class Person {
	private String name;
	private int age;
}
