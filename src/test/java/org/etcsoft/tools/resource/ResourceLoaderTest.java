package org.etcsoft.tools.resource;

import com.fasterxml.jackson.core.JsonParseException;
import org.etcsoft.tools.jackson.factory.ObjectMapperFactory;
import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.etcsoft.tools.exception.UtilsErrorCode.IMPOSSIBLE_SERIALIZE_RESOURCE;
import static org.etcsoft.tools.exception.UtilsErrorCode.LOADING_RESOURCE;

public class ResourceLoaderTest {

	private ResourceLoader resourceHandler = new ResourceLoader(ObjectMapperFactory.objectMapperFactory());

	@Test
	public void whenResourceExist_ThenOk() {
		assertThat(resourceHandler.readResource("test.txt", this.getClass().getClassLoader()))
				.isEqualTo("This is a test");
	}

	@Test
	public void whenJsonResourceExist_ThenOk() {
		assertThat(resourceHandler.readResource("test.json", this.getClass().getClassLoader(), Person.class))
				.hasFieldOrPropertyWithValue("name", "name")
				.hasFieldOrPropertyWithValue("age", 55);
	}

	@Test
	public void whenResourceNotExist_ThenException() {
		assertThatThrownBy(() -> resourceHandler.readResource("testXYZ.txt", this.getClass().getClassLoader()))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("this resource 'testXYZ.txt' is missing")
				.hasFieldOrPropertyWithValue("errorCode", LOADING_RESOURCE)
				.hasCauseInstanceOf(NullPointerException.class);
	}

	@Test
	public void whenResourceNull_ThenException() {
		assertThatThrownBy(() -> resourceHandler.readResource(null, this.getClass().getClassLoader()))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("this resource 'null' is missing")
				.hasFieldOrPropertyWithValue("errorCode", LOADING_RESOURCE)
				.hasCauseInstanceOf(NullPointerException.class);
	}

	@Test
	public void whenClassLoaderNull_ThenException() {
		assertThatThrownBy(() -> resourceHandler.readResource("test.txt", null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("this resource 'test.txt' is missing")
				.hasFieldOrPropertyWithValue("errorCode", LOADING_RESOURCE)
				.hasCauseInstanceOf(NullPointerException.class);
	}

	@Test
	public void whenJsonResourceNotExist_ThenException() {
		assertThatThrownBy(() -> resourceHandler.readResource("testXYZ.json",
															  this.getClass().getClassLoader(),
															  Person.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("this resource 'testXYZ.json' is missing")
				.hasFieldOrPropertyWithValue("errorCode", LOADING_RESOURCE)
				.hasCauseInstanceOf(NullPointerException.class);
	}

	@Test
	public void whenJsonResourceNull_ThenException() {
		assertThatThrownBy(() -> resourceHandler.readResource(null,
															  this.getClass().getClassLoader(),
															  Person.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("this resource 'null' is missing")
				.hasFieldOrPropertyWithValue("errorCode", LOADING_RESOURCE)
				.hasCauseInstanceOf(NullPointerException.class);
	}

	@Test
	public void whenJsonClassLoaderNull_ThenException() {
		assertThatThrownBy(() -> resourceHandler.readResource("test.json", null, Person.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("this resource 'test.json' is missing")
				.hasFieldOrPropertyWithValue("errorCode", LOADING_RESOURCE)
				.hasCauseInstanceOf(NullPointerException.class);
	}

	@Test
	public void whenJsonClassTypeNull_ThenException() {
		assertThatThrownBy(() -> resourceHandler.readResource("test.json",
															  this.getClass().getClassLoader(),
															  null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("This resource 'test.json' cannot be loaded, class type cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", IMPOSSIBLE_SERIALIZE_RESOURCE)
				.hasNoCause();
	}

	@Test
	public void whenJsonResourceCannotBeSerialized_ThenException() {
		assertThatThrownBy(() -> resourceHandler.readResource("wrong.json",
															  this.getClass().getClassLoader(),
															  Person.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessageStartingWith("this resource wrong.json cannot be serialized to this class class")
				.hasMessageEndingWith("Person")
				.hasFieldOrPropertyWithValue("errorCode", IMPOSSIBLE_SERIALIZE_RESOURCE)
				.hasCauseInstanceOf(JsonParseException.class);
	}
}
