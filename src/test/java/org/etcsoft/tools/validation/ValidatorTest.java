package org.etcsoft.tools.validation;

import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;

public class ValidatorTest {
	private final static Map<String, String> MAP_OK = new HashMap<>();
	private final static Map<String, String> MAP_EMPTY_KEY = new HashMap<>();
	private final static Map<String, String> MAP_EMPTY_VALUE = new HashMap<>();
	private final static Map<String, String> MAP_NULL_KEY = new HashMap<>();
	private final static Map<String, String> MAP_NULL_VALUE = new HashMap<>();

	static {
		MAP_OK.put("1", "A");
		MAP_OK.put("2", "B");
		MAP_EMPTY_KEY.put("", "A");
		MAP_EMPTY_KEY.put("2", "B");
		MAP_EMPTY_VALUE.put("1", "A");
		MAP_EMPTY_VALUE.put("2", "");
		MAP_NULL_KEY.put(null, "A");
		MAP_NULL_KEY.put("2", "B");
		MAP_NULL_VALUE.put("1", "A");
		MAP_NULL_VALUE.put("2", null);
	}

	enum Color {
		RED,
		BLUE,
		WHITE;
	}

	private Validator validation = new Validator();

	//**************************************
	//             String
	//**************************************
	@Test
	public void whenInstanceNotBlank_ThenOk() {
		validation.throwIfInstanceBlank(new String("hello"), "NoException", INPUT_ERROR);
	}

	@Test
	public void whenInstanceBlank_ThenException() {
		assertThatThrownBy(() -> validation.throwIfInstanceBlank("", "empty", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("empty")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenInstanceNullInBlank_ThenException() {
		assertThatThrownBy(() -> validation.throwIfInstanceBlank(null, "blank", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	//**************************************
	//             Instances
	//**************************************
	@Test
	public void whenInstanceNotNull_ThenOk() {
		validation.throwIfInstanceNull(new String(""), "NoException", INPUT_ERROR);
	}

	@Test
	public void whenInstanceNull_ThenException() {
		assertThatThrownBy(() -> validation.throwIfInstanceNull(null, "my message", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("my message")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	//**************************************
	//               List
	//**************************************
	@Test
	public void whenListNotContainBlank_ThenOk() {
		validation.throwIfListContainBlank(Arrays.asList("hello", "moto"), "NoException", INPUT_ERROR);
	}

	@Test
	public void whenListContainBlank_ThenException() {
		assertThatThrownBy(() -> validation.throwIfListContainBlank(Arrays.asList("hello", ""), "contain empty", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("contain empty")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> validation.throwIfListContainBlank(Arrays.asList("hello", null), "contain null", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("contain null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenListNotContainNull_ThenOk() {
		validation.throwIfListContainNull(Arrays.asList("hello", "moto"), "NoException", INPUT_ERROR);
	}

	@Test
	public void whenListContainNull_ThenException() {
		assertThatThrownBy(() -> validation.throwIfListContainNull(Arrays.asList("hello", null), "contain null", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("contain null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenListNotEmpty_ThenOk() {
		validation.throwIfListEmpty(Arrays.asList("hello", "moto"), "NoException", INPUT_ERROR);
	}

	@Test
	public void whenListEmpty_ThenException() {
		assertThatThrownBy(() -> validation.throwIfListEmpty(new ArrayList<>(), "empty list", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("empty list")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	//**************************************
	//               MAP
	//**************************************
	@Test
	public void whenMapNotContainBlankKeys_ThenOk() {
		validation.throwIfMapContainBlankKey(MAP_OK, "NoException", INPUT_ERROR);
	}

	@Test
	public void whenMapContainBlankKeys_ThenException() {
		assertThatThrownBy(() -> validation.throwIfMapContainBlankKey(MAP_EMPTY_KEY, "contain empty", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("contain empty")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> validation.throwIfMapContainBlankKey(MAP_NULL_KEY, "contain null", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("contain null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenMapNotContainNullKeys_ThenOk() {
		validation.throwIfMapContainNullKey(MAP_OK, "NoException", INPUT_ERROR);
	}

	@Test
	public void whenMapContainNullKeys_ThenException() {
		assertThatThrownBy(() -> validation.throwIfMapContainNullKey(MAP_NULL_KEY, "contain null", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("contain null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenMapNotContainBlankValues_ThenOk() {
		validation.throwIfMapContainBlankValue(MAP_OK, "NoException", INPUT_ERROR);
	}

	@Test
	public void whenMapContainBlankValue_ThenException() {
		assertThatThrownBy(() -> validation.throwIfMapContainBlankValue(MAP_EMPTY_VALUE, "contain empty", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("contain empty")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> validation.throwIfMapContainBlankValue(MAP_NULL_VALUE, "contain null", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("contain null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenMapNotContainNullValues_ThenOk() {
		validation.throwIfMapContainNullValue(MAP_OK, "NoException", INPUT_ERROR);
	}

	@Test
	public void whenMapContainNullValues_ThenException() {
		assertThatThrownBy(() -> validation.throwIfMapContainNullValue(MAP_NULL_VALUE, "contain null", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("contain null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenMapNotEmpty_ThenOk() {
		validation.throwIfMapEmpty(MAP_OK, "NoException", INPUT_ERROR);
	}

	@Test
	public void whenMapEmpty_ThenException() {
		assertThatThrownBy(() -> validation.throwIfMapEmpty(new HashMap<>(), "empty map", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("empty map")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	//**************************************
	//               Boolean
	//**************************************
	@Test
	public void whenInstanceConditionFalse_ThenOk() {
		validation.throwIfConditionTrue(false, "NoException", INPUT_ERROR);
	}

	@Test
	public void whenInstanceConditionTrue_ThenException() {
		assertThatThrownBy(() -> validation.throwIfConditionTrue(true, "the condition was true", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("the condition was true")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	//**************************************
	//               ENUMS
	//**************************************
	@Test
	public void whenStringInEnum_ThenOk() {
		validation.throwIfStringIsNotInEnum(Color.class, "red", "NoException", INPUT_ERROR);
	}

	@Test
	public void whenStringNotInEnum_ThenException() {
		assertThatThrownBy(() -> validation.throwIfStringIsNotInEnum(Color.class, "nocolor", "not enum", INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("not enum")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenStringCaseSensitiveInEnum_ThenOk() {
		validation.throwIfStringIsNotInEnumCaseSensitive(Color.class, "RED", "NoException", INPUT_ERROR);
	}

	@Test
	public void whenStringNotCaseSensitiveInEnum_ThenException() {
		assertThatThrownBy(() -> validation.throwIfStringIsNotInEnumCaseSensitive(Color.class,
																				  "ReD",
																				  "not enum",
																				  INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("not enum")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenStringCaseSensitiveNotInEnum_ThenException() {
		assertThatThrownBy(() -> validation.throwIfStringIsNotInEnumCaseSensitive(Color.class,
																				  "nocolor",
																				  "not enum",
																				  INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("not enum")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	//**************************************
	//               PORTS
	//**************************************
	@Test
	public void whenValidPort_ThenOk() {
		validation.throwIfInvalidPortValue(3555, INPUT_ERROR);
	}

	@Test
	public void whenPortNull_ThenException() {
		assertThatThrownBy(() -> validation.throwIfInvalidPortValue(null, INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("a port cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenInvalidPort_ThenException() {
		assertThatThrownBy(() -> validation.throwIfInvalidPortValue(-5, INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("This port '-5' has a wrong range value.")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> validation.throwIfInvalidPortValue(75555, INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("This port '75555' has a wrong range value.")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR);

		assertThatThrownBy(() -> validation.throwIfInvalidPortValue(0, INPUT_ERROR))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("This port '0' has a wrong range value.")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR);
	}

	@Test
	public void whenValidPort_ThenTrue() {
		assertThat(validation.isValidPortValue(555)).isEqualTo(true);
	}

	@Test
	public void whenPortNull_ThenFalse() {
		assertThat(validation.isValidPortValue(null)).isEqualTo(false);
	}

	@Test
	public void whenInvalidPort_ThenFalse() {
		assertThat(validation.isValidPortValue(-55)).isEqualTo(false);
		assertThat(validation.isValidPortValue(75555)).isEqualTo(false);
		assertThat(validation.isValidPortValue(0)).isEqualTo(false);
	}

	public boolean isValidPortValue(Integer port) {
		return !(port == null || port<2 || port >65535);
	}
}
