package org.etcsoft.tools.utc;

import org.junit.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;

public class JVMUtcTimeZoneSetterTest {

	@Test
	public void whenSetUtcTimeZoneAsDefault_ThenOk() {
		new JVMUtcTimeZoneSetter().setUtcTimeZoneAsDefault();
		assertThat(OffsetDateTime.now().getOffset()).isEqualTo(ZoneOffset.UTC);
	}
}
