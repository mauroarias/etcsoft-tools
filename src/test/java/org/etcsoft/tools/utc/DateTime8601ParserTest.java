package org.etcsoft.tools.utc;

import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.etcsoft.tools.exception.UtilsErrorCode.PARSING_ERROR;

@RunWith(Theories.class)
public class DateTime8601ParserTest {
	private DateTime8601Parser passer = new DateTime8601Parser();

	@DataPoints
	public static String[] dates = {
			"2014-01-01T21:59:55.555Z",
			"2014-01-01T21:59:55Z",
			"2014-01-01T21:59:55.55Z",
			"2014-01-01T21:59:55.5Z",
			"2014-01-01T21:59Z",
			"2014-01-01T21:59:55.555+04:00",
			"2014-01-01T21:59:55.55+04:00",
			"2014-01-01T21:59:55.5+04:00",
			"2014-01-01T21:59:55+04:00",
			"2014-01-01T21:59+04:00",
			"2014-01-01 21:59:55.555+04:00",
			"2014-01-01 21:59:55.55+04:00",
			"2014-01-01 21:59:55.5+04:00",
			"2014-01-01 21:59+04:00" };

	private static Map<String, OffsetDateTime> goodOffsetExpected = new HashMap() {{
		put("2014-01-01T21:59:55.555Z", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 555000000, ZoneOffset.UTC));
		put("2014-01-01T21:59:55Z", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 0, ZoneOffset.UTC));
		put("2014-01-01T21:59:55.55Z", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 550000000, ZoneOffset.UTC));
		put("2014-01-01T21:59:55.5Z", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 500000000, ZoneOffset.UTC));
		put("2014-01-01T21:59Z", OffsetDateTime.of(2014, 01, 01, 21, 59, 0, 0, ZoneOffset.UTC));
		put("2014-01-01T21:59:55.555+04:00", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 555000000, ZoneOffset.of("+04:00")));
		put("2014-01-01T21:59:55.55+04:00", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 550000000, ZoneOffset.of("+04:00")));
		put("2014-01-01T21:59:55.5+04:00", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 500000000, ZoneOffset.of("+04:00")));
		put("2014-01-01T21:59:55+04:00", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 0, ZoneOffset.of("+04:00")));
		put("2014-01-01T21:59+04:00", OffsetDateTime.of(2014, 01, 01, 21, 59, 0, 0, ZoneOffset.of("+04:00")));
		put("2014-01-01 21:59:55.555+04:00", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 555000000, ZoneOffset.of("+04:00")));
		put("2014-01-01 21:59:55.55+04:00", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 550000000, ZoneOffset.of("+04:00")));
		put("2014-01-01 21:59:55.5+04:00", OffsetDateTime.of(2014, 01, 01, 21, 59, 55, 500000000, ZoneOffset.of("+04:00")));
		put("2014-01-01 21:59+04:00", OffsetDateTime.of(2014, 01, 01, 21, 59, 0, 0, ZoneOffset.of("+04:00")));
	}};

	private static Map<String, OffsetDateTime> goodLocalExpected = new HashMap() {{
		put("2014-01-01T21:59:55.555Z", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 555000000));
		put("2014-01-01T21:59:55Z", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 0));
		put("2014-01-01T21:59:55.55Z", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 550000000));
		put("2014-01-01T21:59:55.5Z", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 500000000));
		put("2014-01-01T21:59Z", LocalDateTime.of(2014, 01, 01, 21, 59, 0, 0));
		put("2014-01-01T21:59:55.555+04:00", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 555000000));
		put("2014-01-01T21:59:55.55+04:00", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 550000000));
		put("2014-01-01T21:59:55.5+04:00", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 500000000));
		put("2014-01-01T21:59:55+04:00", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 0));
		put("2014-01-01T21:59+04:00", LocalDateTime.of(2014, 01, 01, 21, 59, 0, 0));
		put("2014-01-01 21:59:55.555+04:00", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 555000000));
		put("2014-01-01 21:59:55.55+04:00", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 550000000));
		put("2014-01-01 21:59:55.5+04:00", LocalDateTime.of(2014, 01, 01, 21, 59, 55, 500000000));
		put("2014-01-01 21:59+04:00", LocalDateTime.of(2014, 01, 01, 21, 59, 0, 0));
	}};

	@Theory
	public void whenParsingGoodDatesToOffsetDateTime_ThenOk(String date) {

		assertThat(passer.parseToOffsetDateTime(date)).isEqualTo(goodOffsetExpected.get(date));
	}

	@Theory
	public void whenParsingGoodDatesToLocalDateTime_ThenOk(String date) {
		assertThat(passer.parseToLocalDateTime(date)).isEqualTo(goodLocalExpected.get(date));
	}

	@Test
	public void whenParsingNullOffsetDateTime_ThenException() {

		assertThatThrownBy(() -> passer.parseToOffsetDateTime(null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("blank date cannot be parsed to datetime")
				.hasFieldOrPropertyWithValue("errorCode", PARSING_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenParsingEmptyOffsetDateTime_ThenException() {
		assertThatThrownBy(() -> passer.parseToOffsetDateTime(""))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("blank date cannot be parsed to datetime")
				.hasFieldOrPropertyWithValue("errorCode", PARSING_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenParsingWrongFormatOffsetDateTime_ThenException() {
		assertThatThrownBy(() -> passer.parseToOffsetDateTime("wrongFormat"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("This date 'wrongFormat' cannot be parser to datetime")
				.hasFieldOrPropertyWithValue("errorCode", PARSING_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenParsingNullLocalDateTime_ThenException() {
		assertThatThrownBy(() -> passer.parseToLocalDateTime(null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("blank date cannot be parsed to datetime")
				.hasFieldOrPropertyWithValue("errorCode", PARSING_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenParsingEmptyLocalDateTime_ThenNull() {
		assertThatThrownBy(() -> passer.parseToLocalDateTime(""))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("blank date cannot be parsed to datetime")
				.hasFieldOrPropertyWithValue("errorCode", PARSING_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenParsingWrongFormatLocalDateTime_ThenNull() {
		assertThatThrownBy(() -> passer.parseToLocalDateTime("wrongFormat"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("This date 'wrongFormat' cannot be parser to datetime")
				.hasFieldOrPropertyWithValue("errorCode", PARSING_ERROR)
				.hasNoCause();
	}
}
