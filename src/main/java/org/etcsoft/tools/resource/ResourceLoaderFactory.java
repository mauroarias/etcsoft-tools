package org.etcsoft.tools.resource;

import org.etcsoft.tools.jackson.factory.ObjectMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceLoaderFactory {
	private final static Logger logger = LoggerFactory.getLogger(ResourceLoaderFactory.class);
	private final static ResourceLoaderFactory factory = new ResourceLoaderFactory();

	private final ResourceLoader resourceLoader;

	public ResourceLoaderFactory() {
		resourceLoader = new ResourceLoader(ObjectMapperFactory.objectMapperFactory());

		logger.info("Configuring Resource loader bean");
	}

	public static ResourceLoader resourceLoaderFactory() {
		return factory.getResourceLoader();
	}

	public ResourceLoader getResourceLoader() {
		return resourceLoader;
	}
}
