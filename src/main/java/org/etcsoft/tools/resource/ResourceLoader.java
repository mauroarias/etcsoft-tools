package org.etcsoft.tools.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static java.lang.String.format;
import static org.etcsoft.tools.exception.UtilsErrorCode.IMPOSSIBLE_SERIALIZE_RESOURCE;
import static org.etcsoft.tools.exception.UtilsErrorCode.LOADING_RESOURCE;

public class ResourceLoader {

	private final static Logger logger = LoggerFactory.getLogger(ResourceLoader.class);
	private final Validator validation = new Validator();

	private final ObjectMapper mapper;

	public ResourceLoader(final ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public String readResource(final String resourceFile, final ClassLoader classLoader) {
		try {
			logger.debug(format("loading resource '%s' file", resourceFile));
			final InputStream is = classLoader.getResourceAsStream(resourceFile);
			final InputStreamReader isr = new InputStreamReader(is);
			final BufferedReader br = new BufferedReader(isr);
			final StringBuilder sb = new StringBuilder();
			String line;
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			isr.close();
			is.close();
			return sb.toString();
		}
		catch(Exception cause) {
			throw new EtcsoftException(LOADING_RESOURCE, format("this resource '%s' is missing", resourceFile), cause);
		}
	}

	public <T> T readResource(final String resourceFile, final ClassLoader classLoader, final Class<T> clazz) {

		validation.throwIfInstanceNull(clazz,
									   format("This resource '%s' cannot be loaded, class type cannot be null",
											  resourceFile),
									   IMPOSSIBLE_SERIALIZE_RESOURCE);

		try {
			return mapper.readValue(readResource(resourceFile, classLoader), clazz);

		} catch(EtcsoftException ex) {

			throw ex;
		} catch(Exception cause) {

			throw new EtcsoftException(IMPOSSIBLE_SERIALIZE_RESOURCE,
									   format("this resource %s cannot be serialized to this class %s",
											  resourceFile,
											  clazz.toString()),
									   cause);
		}
	}
}
