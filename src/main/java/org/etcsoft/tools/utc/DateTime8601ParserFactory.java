package org.etcsoft.tools.utc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateTime8601ParserFactory {
	private final static Logger logger = LoggerFactory.getLogger(DateTime8601ParserFactory.class);
	private final static DateTime8601ParserFactory factory = new DateTime8601ParserFactory();

	private final DateTime8601Parser dateTime8601Parser;

	public DateTime8601ParserFactory() {
		dateTime8601Parser = new DateTime8601Parser();
		logger.info("datetime8601 Object mapper bean");
	}

	public static DateTime8601Parser dateTime8601ParserFactory() {
		return factory.getDateTime8601Parser();
	}

	public DateTime8601Parser getDateTime8601Parser() {
		return dateTime8601Parser;
	}
}
