package org.etcsoft.tools.utc;

import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.validation.Validator;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;
import static org.etcsoft.tools.exception.UtilsErrorCode.PARSING_ERROR;

public class DateTime8601Parser {

	private final static List<DateTimeFormatter> DATETIME_UTC_FORMATTER_PARSE_LIST =
			Arrays.asList(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SS'Z'"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.S'Z'"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm'Z'"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss'Z'"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSXXX"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SXXX"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mmXXX"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss.SSSXXX"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss.SSXXX"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss.SXXX"),
						  DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mmXXX"));

	private final Validator validation = new Validator();

	public OffsetDateTime parseToOffsetDateTime(final String date) {

		for(DateTimeFormatter formatter : DATETIME_UTC_FORMATTER_PARSE_LIST) {
			try {
				return OffsetDateTime.parse(date, formatter);
			}
			catch(Exception e) {
				/* Bad formatter, proceed to next one */
			}
		}

		validation.throwIfInstanceBlank(date, "blank date cannot be parsed to datetime", PARSING_ERROR);
		throw new EtcsoftException(PARSING_ERROR, format("This date '%s' cannot be parser to datetime", date));
	}

	public LocalDateTime parseToLocalDateTime(final String date) {

		for(DateTimeFormatter formatter : DATETIME_UTC_FORMATTER_PARSE_LIST) {
			try {
				return LocalDateTime.parse(date, formatter);
			}
			catch(Exception e) {
				/* Bad formatter, proceed to next one */
			}
		}

		validation.throwIfInstanceBlank(date, "blank date cannot be parsed to datetime", PARSING_ERROR);
		throw new EtcsoftException(PARSING_ERROR, format("This date '%s' cannot be parser to datetime", date));
	}
}
