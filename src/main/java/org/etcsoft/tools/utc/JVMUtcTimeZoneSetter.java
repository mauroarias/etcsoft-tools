package org.etcsoft.tools.utc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimeZone;

public class JVMUtcTimeZoneSetter {

	private final static Logger logger = LoggerFactory.getLogger(JVMUtcTimeZoneSetter.class);

	public void setUtcTimeZoneAsDefault() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		logger.info("configuring JVM's time zone to UTC");
	}
}
