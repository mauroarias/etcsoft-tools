package org.etcsoft.tools.exception;

import org.etcsoft.tools.model.HttpStatus;

public interface ErrorCode {
	HttpStatus getHttpStatus();
}
