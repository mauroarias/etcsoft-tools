package org.etcsoft.tools.exception;

import lombok.Getter;

import static org.etcsoft.tools.exception.UtilsErrorCode.UNKNOWN_ERROR;

public class EtcsoftException extends RuntimeException {

	@Getter
	private final ErrorCode errorCode;

	public EtcsoftException(final String message) {
		super(message);
		this.errorCode = UNKNOWN_ERROR;
	}

	public EtcsoftException(final ErrorCode errorCode, final String message) {
		super(message);
		if(errorCode == null) {
			this.errorCode = UNKNOWN_ERROR;
		}
		else {
			this.errorCode = errorCode;
		}
	}

	public EtcsoftException(final String message, final Throwable cause) {
		super(message, cause);
		this.errorCode = UNKNOWN_ERROR;
	}

	public EtcsoftException(final ErrorCode errorCode, final String message, final Throwable cause) {
		super(message, cause);
		if(errorCode == null) {
			this.errorCode = UNKNOWN_ERROR;
		}
		else {
			this.errorCode = errorCode;
		}
	}
}
