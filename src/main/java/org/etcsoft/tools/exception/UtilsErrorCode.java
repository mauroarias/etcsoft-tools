package org.etcsoft.tools.exception;

import lombok.Getter;
import org.etcsoft.tools.model.HttpStatus;

import static org.etcsoft.tools.model.HttpStatus.BAD_REQUEST;
import static org.etcsoft.tools.model.HttpStatus.INTERNAL_SERVER_ERROR;

public enum UtilsErrorCode implements ErrorCode {
	INPUT_ERROR(BAD_REQUEST),
	PARSING_ERROR(INTERNAL_SERVER_ERROR),
	LOADING_RESOURCE(INTERNAL_SERVER_ERROR),
	UNKNOWN_ERROR(INTERNAL_SERVER_ERROR),
	IMPOSSIBLE_SERIALIZE_RESOURCE(INTERNAL_SERVER_ERROR);

	@Getter
	private final HttpStatus httpStatus;

	UtilsErrorCode(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
