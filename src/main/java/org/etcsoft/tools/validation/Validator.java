package org.etcsoft.tools.validation;

import org.apache.commons.lang3.StringUtils;
import org.etcsoft.tools.exception.ErrorCode;
import org.etcsoft.tools.exception.EtcsoftException;

import java.util.Collection;
import java.util.Map;

import static java.lang.String.format;

public class Validator {

	//**************************************
	//             String
	//**************************************
	public void throwIfInstanceBlank(final String instance, final String message, final ErrorCode errorCode) {
		if(StringUtils.isBlank(instance)) {
			throw new EtcsoftException(errorCode, message);
		}
	}

	//**************************************
	//             Instances
	//**************************************
	public void throwIfInstanceNull(final Object instance, final String message, final ErrorCode errorCode) {
		if(instance == null) {
			throw new EtcsoftException(errorCode, message);
		}
	}

	//**************************************
	//               List
	//**************************************
	public void throwIfListContainNull(final Collection<?> list,
									   final String message,
									   final ErrorCode errorCode) {
		if(list != null) {
			if(list.contains(null)) {
				throw new EtcsoftException(errorCode, message);
			}
		}
	}

	public void throwIfListContainBlank(final Collection<String> list,
										final String message,
										final ErrorCode errorCode) {
		if(list != null) {
			if(list.contains(null) || list.contains("")) {
				throw new EtcsoftException(errorCode, message);
			}
		}
	}

	public void throwIfListEmpty(final Collection<?> list,
								 final String message,
								 final ErrorCode errorCode) {
		if(list != null) {
			if(list.isEmpty()) {
				throw new EtcsoftException(errorCode, message);
			}
		}
	}

	//**************************************
	//               MAP
	//**************************************
	public void throwIfMapContainBlankKey(final Map<String, ?> map, final String message, final ErrorCode errorCode) {
		if(map != null) {
			if(map.containsKey(null) || map.containsKey("")) {
				throw new EtcsoftException(errorCode, message);
			}
		}
	}

	public void throwIfMapContainNullKey(final Map<?, ?> map, final String message, final ErrorCode errorCode) {
		if(map != null) {
			if(map.containsKey(null)) {
				throw new EtcsoftException(errorCode, message);
			}
		}
	}

	public void throwIfMapContainBlankValue(final Map<?, String> map, final String message, final ErrorCode errorCode) {
		if(map != null) {
			if(map.containsValue(null) || map.containsValue("")) {
				throw new EtcsoftException(errorCode, message);
			}
		}
	}

	public void throwIfMapContainNullValue(final Map<?, ?> map, final String message, final ErrorCode errorCode) {
		if(map != null) {
			if(map.containsValue(null)) {
				throw new EtcsoftException(errorCode, message);
			}
		}
	}

	public void throwIfMapEmpty(final Map<?, ?> map, final String message, final ErrorCode errorCode) {
		if(map != null) {
			if(map.isEmpty()) {
				throw new EtcsoftException(errorCode, message);
			}
		}
	}

	//**************************************
	//               Boolean
	//**************************************
	public void throwIfConditionTrue(final boolean condition, final String message, final ErrorCode errorCode) {
		if(condition) {
			throw new EtcsoftException(errorCode, message);
		}
	}

	//**************************************
	//               ENUMS
	//**************************************
	public <E extends Enum<E>> void throwIfStringIsNotInEnum(final Class<E> enumeration,
															 final String stringValue,
															 final String message,
															 final ErrorCode errorCode) {
		throwIfStringIsNotInEnumCaseSensitive(enumeration, stringValue.toUpperCase(), message, errorCode);
	}

	public <E extends Enum<E>> void throwIfStringIsNotInEnumCaseSensitive(final Class<E> enumeration,
																		  final String stringValue,
																		  final String message,
																		  final ErrorCode errorCode) {
		try {
			Enum.valueOf(enumeration, stringValue);
		}
		catch(IllegalArgumentException ex) {
			throw new EtcsoftException(errorCode, message);
		}
	}

	//**************************************
	//               PORTS
	//**************************************
	public void throwIfInvalidPortValue(final Integer port, final ErrorCode errorCode) {
		if(port == null) {
			throw new EtcsoftException(errorCode, "a port cannot be null");
		}
		if(port < 2 || port > 65535) {
			throw new EtcsoftException(errorCode, format("This port '%d' has a wrong range value.", port));
		}
	}

	public boolean isValidPortValue(final Integer port) {
		return !(port == null || port < 2 || port > 65535);
	}
}
