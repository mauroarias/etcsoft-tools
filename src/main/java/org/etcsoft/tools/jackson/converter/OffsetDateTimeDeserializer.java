package org.etcsoft.tools.jackson.converter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.etcsoft.tools.utc.DateTime8601Parser;
import org.etcsoft.tools.utc.DateTime8601ParserFactory;

import java.io.IOException;
import java.time.OffsetDateTime;

public class OffsetDateTimeDeserializer extends JsonDeserializer<OffsetDateTime> {

	private final DateTime8601Parser dateTime8601Parser = DateTime8601ParserFactory.dateTime8601ParserFactory();

	@Override
	public OffsetDateTime deserialize(final JsonParser jsonParser,
									  final DeserializationContext deserializationContext) throws IOException {
		return dateTime8601Parser.parseToOffsetDateTime(jsonParser.getText());
	}
}
