package org.etcsoft.tools.jackson.converter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.sql.Timestamp;

public class TimeStampSerializer extends JsonSerializer<Timestamp> {

	@Override
	public void serialize(final Timestamp value,
						  final JsonGenerator gen,
						  final SerializerProvider serializers) throws IOException {
		gen.writeString(value.toLocalDateTime().toString());
	}
}
