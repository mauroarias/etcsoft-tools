package org.etcsoft.tools.jackson.converter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.OffsetDateTime;

public final class OffsetDateTimeSerializer extends JsonSerializer<OffsetDateTime> {

	@Override
	public void serialize(final OffsetDateTime value,
						  final JsonGenerator gen,
						  final SerializerProvider serializers) throws IOException {
		gen.writeString(value.toString());
	}
}