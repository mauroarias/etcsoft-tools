package org.etcsoft.tools.jackson.factory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.etcsoft.tools.jackson.converter.OffsetDateTimeSerializer;
import org.etcsoft.tools.jackson.converter.TimeStampSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.time.OffsetDateTime;

public class ObjectMapperFactory {

	private final static Logger logger = LoggerFactory.getLogger(ObjectMapperFactory.class);
	private final static ObjectMapperFactory factory = new ObjectMapperFactory();

	private final ObjectMapper objectMapper;

	public ObjectMapperFactory() {
		objectMapper = new ObjectMapper();
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		objectMapper.configure(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS, true);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		//java 8 Datetime
		SimpleModule javaTime = new JavaTimeModule();
		javaTime.addSerializer(OffsetDateTime.class, new OffsetDateTimeSerializer());
		javaTime.addSerializer(Timestamp.class, new TimeStampSerializer());
		objectMapper.registerModule(javaTime);

		logger.info("Configuring Object mapper bean");
	}

	public static ObjectMapper objectMapperFactory() {
		return factory.getObjectMapper();
	}

	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}
}
