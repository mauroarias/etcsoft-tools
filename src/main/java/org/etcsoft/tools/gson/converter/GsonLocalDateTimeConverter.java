package org.etcsoft.tools.gson.converter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.etcsoft.tools.utc.DateTime8601Parser;

import java.lang.reflect.Type;
import java.time.LocalDateTime;

public class GsonLocalDateTimeConverter implements JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {

	private final DateTime8601Parser dateTime8601Passing = new DateTime8601Parser();

	@Override
	public JsonElement serialize(final LocalDateTime src, final Type srcType, final JsonSerializationContext context) {
		return new JsonPrimitive(src.toString());
	}

	@Override
	public LocalDateTime deserialize(final JsonElement json, final Type type, final JsonDeserializationContext context)
			throws JsonParseException {
		return dateTime8601Passing.parseToLocalDateTime(json.getAsString());
	}
}
