package org.etcsoft.tools.gson.converter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.time.LocalDate;

public class GsonLocalDateConverter implements JsonSerializer<LocalDate>, JsonDeserializer<LocalDate> {

	@Override
	public JsonElement serialize(final LocalDate src, final Type srcType, final JsonSerializationContext context) {
		return new JsonPrimitive(src.toString());
	}

	@Override
	public LocalDate deserialize(final JsonElement json, final Type type, final JsonDeserializationContext context)
			throws JsonParseException {
		return LocalDate.parse(json.getAsString());
	}
}
