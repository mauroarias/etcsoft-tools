package org.etcsoft.tools.gson.factory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.etcsoft.tools.gson.converter.GsonLocalDateConverter;
import org.etcsoft.tools.gson.converter.GsonLocalDateTimeConverter;
import org.etcsoft.tools.gson.converter.GsonOffsetDateTimeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

public class GsonFactory {

	private final static Logger logger = LoggerFactory.getLogger(GsonFactory.class);
	private final static GsonFactory factory = new GsonFactory();

	private final Gson gson;

	public GsonFactory() {
		gson = new GsonBuilder()
				.registerTypeAdapter(OffsetDateTime.class, new GsonOffsetDateTimeConverter())
				.registerTypeAdapter(LocalDate.class, new GsonLocalDateConverter())
				.registerTypeAdapter(LocalDateTime.class, new GsonLocalDateTimeConverter())
				.create();
		logger.info("Configuring Gson bean");
	}

	public Gson getGson() {
		return gson;
	}

	public Gson gsonFactory() {
		return factory.getGson();
	}
}
