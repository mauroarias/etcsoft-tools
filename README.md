# Tools library

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/org.etcsoft/etcsoft-tools/badge.svg)](https://maven-badges.herokuapp.com/maven-central/org.etcsoft/etcsoft-tools)
[![Build Status](https://travis-ci.org/mauroarias/etcsoft-tools.svg?branch=master)](https://travis-ci.org/mauroarias/etcsoft-tools)

This library offers several base tools as:

## EXCEPTION:
Create an etcsoft base Exception on the project adding an errorcode type with a http embbeded error attached, it extends from **RuntimeException**.

## GSON:
### factory:
This gives access to Gson serializers & deserializers.
Use as:
```
private Gson gson = GsonFactory.gsonFactory();
```

### Converters:
converters used by Jackson and spring as:
* offsetdatetime converter.
* LocalDate converter.
* LocalDateTime converter.

## JACKSON:
### factory:
This gives access to Object mapper class with a base configuration and some custom serializers & deserializers.
Use as:
```
private ObjectMapper mapper = ObjectMapperFactory.objectMapperFactory();
```

### Deserializer:
converters used by Jackson and spring as:
* offsetdatetime converter.

### Serializer:
serializers used by Jackson and spring as:
* OffsetDataTime Jackson serializer.
* TimeStamp Jackson serializer.

## RESOURCE:
### Resource loader:
This gives parsing to read files from resources. 
```
private ResourceLoader = ResourceLoaderFactory.resourceLoaderFactory();
```

* LOAD A String OR TEXT FILE FROM RESOURCE:
Example:
```
private ResourceLoader = ResourceLoaderFactory.resourceLoaderFactory();
String resourceFromString = resourceLoader.readResource("resourceFile.txt", this.getClass().getClassLoader());
```

* LOAD A GENERIC INSTANCE FROM A json RESOURCE FILE:
Example:
```
private ResourceLoader = ResourceLoaderFactory.resourceLoaderFactory();
Myclass myclass = resourceLoader.readResource("file.json", this.getClass().getClassLoader(), Myclass.class);
```

## UTC:
### JVM Utc time zone setter
This configures the JVM to use UTC time zone.
```
new JVMUtcTimeZoneSetter.setUtcTimeZoneAsDefault();
```

### DateTime ISO8601 passer
This gives parsing support to ISO 8601. 
Use Autowired annotation to load the instance. Example:
```
private DateTime8601Parser datetimeParser = DateTime8601ParserFactory.dateTime8601ParserFactory()
```

Note that the pattern supported are:
```
yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
yyyy-MM-dd'T'HH:mm:ss'Z'
yyyy-MM-dd'T'HH:mm:ss.SS'Z'
yyyy-MM-dd'T'HH:mm:ss.S'Z'
yyyy-MM-dd'T'HH:mm'Z'
yyyy-MM-dd' 'HH:mm:ss'Z'
yyyy-MM-dd'T'HH:mm:ss.SSSXXX
yyyy-MM-dd'T'HH:mm:ss.SSXXX
yyyy-MM-dd'T'HH:mm:ss.SXXX
yyyy-MM-dd'T'HH:mm:ssXXX
yyyy-MM-dd'T'HH:mmXXX
yyyy-MM-dd' 'HH:mm:ss.SSSXXX
yyyy-MM-dd' 'HH:mm:ss.SSXXX
yyyy-MM-dd' 'HH:mm:ss.SXXX
yyyy-MM-dd' 'HH:mmXXX"
```

* PARSE A String TO java.time.OffsetDateTime:
Example:
```
private DateTime8601Parser datetimeParser = DateTime8601ParserFactory.dateTime8601ParserFactory()
OffsetDateTime dateTimeParsed = datetimeParser.parseToOffsetDateTime("1991-01-01'T'12:00'Z'");
```

* PARSE A String TO java.time.LocalDateTime:
Example:
```
private DateTime8601Parser datetimeParser = DateTime8601ParserFactory.dateTime8601ParserFactory()
LocalDateTime dateTimeParsed = datetimeParser.parseToLocalDateTime("1991-01-01'T'12:00'Z'");
```

## VALIDATION:
### Validator:
This gives support to validate beans' fields.
Use as:
```
private Validator validator = new Validator();
```

* thrown an IllegalException with a specific errorCode if the instance is blank.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
validator.throwIfInstanceBlank(myInstance, "message in the exception and log", errorCode);
```

* thrown an IllegalException with a specific errorCode if the instance is null.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
validator.throwIfInstanceNull(myInstance, "message in the exception and log", errorCode);
```

* thrown an IllegalException with a specific errorCode if the collection contains null instances.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
validator.throwIfListContainNullInstance(myColectionInstance, "message in the exception and log", errorCode);
```

* thrown an IllegalException with a specific errorCode if the collection contains blank strings.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
validator.throwIfListContainBlankInstance(myColectionInstance, "message in the exception and log", errorCode);
```
  
* thrown an IllegalException with a specific errorCode if the Map contains blank String keys.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
validator.throwIfMapContainBlankKey(myMapInstance, "message in the exception and log", errorCode);
```
  
* thrown an IllegalException with a specific errorCode if the Map contains null keys.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
validator.throwIfMapContainNullKey(myMapInstance, "message in the exception and log", errorCode);
```
  
* thrown an IllegalException with a specific errorCode if the Map contains blank String values.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
validator.throwIfMapContainBlankValue(myMapInstance, "message in the exception and log", errorCode);
```
  
* thrown an IllegalException with a specific errorCode if the Map contains null values.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
validator.throwIfMapContainNullValue(myMapInstance, "message in the exception and log", errorCode);
```
  
* thrown an IllegalException with a specific errorCode if the condition is true.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
private Boolean condition = true;
validator.throwIfConditionTrue(condition, "message in the exception and log", errorCode);
```
  
* thrown an IllegalException with a specific errorCode if the value is not a valid number IP port.
Example:
```
private Validator validator = new Validator();
private UtilsErrorCode errorCode = INPUT_ERROR;
private Integer port = 3306;
validator.throwIfInvalidPortValue(port, errorCode);
```
  
* check if the value is a valid number IP port.
Example:
```
private Integer port = 3306;
validator.isValidPortValue(port);
```

## Compiling 

### running unit test
```
mvn clean install
```

### skipping unit tests
```
mvn clean install -DskipTests
```
